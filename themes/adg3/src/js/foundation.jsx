require('../sass/app.scss')
import 'script!jquery'
import { foundation } from 'script!foundation-sites'

$(document).ready(function(){
  $(document).foundation();
  var el = new Foundation.DropdownMenu($('.dropdown'), {});
  var elem = new Foundation.Orbit($('#orbit'), {});

  window.ATL_JQ_PAGE_PROPS =  {
    "triggerFunction": function(showCollectorDialog) { 
        $(".report").click(function(e) {
            e.preventDefault();
            showCollectorDialog();
        });
  }};



      (function ($) {
          var timeouts;
          timeouts = [];
          $('#join-list').on('click', function (e) {
              var emailField = $('#email'),
              email = emailField.val(),
              joinButton = $('#join-list'),
              signupMessage = $('#signup-message');
              if (email !== "" && email.indexOf('@') > -1) {
                  $.post('https://hamlet.atlassian.com/1.0/public/email/' + email + '/subscribe?mailingListId=1243499');
                  emailField.hide();
                  joinButton.hide();
                  signupMessage.html('Thank you for signing up! A confirmation email has been sent.').addClass('success').show(400);
              } else {
                  signupMessage.html('Please enter a valid email address.').addClass('warning').show(400);
              }
          });
          return $('#email').on('keyup', function () {
              var signupMessage, signupMessageVisible;
              signupMessage = $('#signup-message');
              signupMessageVisible = signupMessage.is(':visible');
              if (event.keyCode === 13) {
                  $('#join-list').click();
              } else if (signupMessageVisible) {
                  signupMessage.hide(400).html('');
              }
          });
      }(jQuery));



});
