FROM ubuntu:xenial

# Install SASS, Node/NPM, and misc utils
RUN apt-get update && \
    apt-get install -y python3 virtualenv \
                       libsass0 nodejs npm  \
                       curl git && \
    apt-get clean

RUN ln -s /usr/bin/nodejs /usr/bin/node


# Install Hugo
ENV HUGO_VERSION 0.16
ENV HUGO_TAR hugo_${HUGO_VERSION}_linux-64bit.tgz

WORKDIR /tmp/
RUN mkdir hugo && cd hugo && \
    curl -L https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/${HUGO_TAR} -o ${HUGO_TAR} && \
    tar xzf ${HUGO_TAR} && \
    cp hugo /usr/local/bin/ && \
    cd .. && rm -rf hugo
