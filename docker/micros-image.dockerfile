# Note: This Docker image is used to deploy an Apache instance on our internal
#       PaaS. It is of no use outside of Atlassian.

FROM debian:jessie

RUN apt-get update && \
    apt-get -y install apache2 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Setup Apache
COPY micros/apache2 /etc/apache2
RUN a2enconf micros-logging \
             servername \
             error-pages \
             data-compression \
             server-worker \
             general-performance \
             robots-control \
             legacydac-feed-proxy \
             jiracloud-redirects.conf
RUN a2enmod rewrite \
            headers \
            ssl \
            log_forensic
RUN a2dismod -f autoindex

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data

EXPOSE 8080
EXPOSE 8081


# Install content
COPY public /var/www/html
RUN echo '{"status":"OK"}' > /var/www/html/healthcheck.json


CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND", "-e", "info"]
