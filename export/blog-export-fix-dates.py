#!/usr/bin/env python

import re
import argparse
import logging as log


def get_options():
    parser = argparse.ArgumentParser(description='fixes date formats')
    parser.add_argument('target', metavar='target', help='the filename')
    parser.add_argument('-o', '--output', help='file to write to')
    return parser.parse_args()


if __name__ == '__main__':
    options = get_options()
    lines = open(options.target).readlines()
    out = open(options.output, 'w')
    result = []
    hiding = False
    log.info("parsing", options.target)
    log.info("rewriting", options.output)
    for l in lines:
        if re.search("^date:\s+\"\d\d\d\d-\d\d-\d\d\s+\d+:\d\d\"", l):

            result.append(re.sub(
                "(^date:\s+\"\d\d\d\d-\d\d-\d\d)(\s+)(\d+:\d\d)",
                "\\1T\\3:00+07:00", l))
            log.info("match", result[-1])
        else:
            result.append(l)
    out.write(''.join(result))

