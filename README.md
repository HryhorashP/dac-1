# Atlassian developer documentation

This repository is the codebase for developer.atlassian.com (DAC). 
 It contains content, themes, templates, and configuration for running 
 a local server (Hugo) for static content that is generated from static 
 files.

## Contributors

Pull requests, issues, and comments welcome. For pull requests:

* add tests for new features and bug fixes
* follow the existing style
* separate unrelated changes into multiple pull requests

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code, documentation, or translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions, we ask that you please follow the  
appropriate link below to digitally sign the CLA. The Corporate CLA is for 
those who are contributing as a member of an organization and the individual 
CLA is for those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

## License

Copyright (c) 2016 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

## Development setup

The setup instructions below are for users who are developing features or
are interested in running DAC server locally on their machine. You do not
need to complete the development setup or run DAC locally if you are using 
the "Improve this page" function to update content.

You need access to a command line or terminal of your choice. 

We recommend using [Homebrew](http://brew.sh/) to install and manage the 
software listed below. Homebrew is a command line friendly package management 
tool that makes installing and maintaining packages way simpler. 

To install Homebrew, run the following command in Terminal:

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

The following software is required to run DAC locally:

* [Pandoc](http://pandoc.org/) 
* [Hugo](https://gohugo.io/)
* [Python3](https://www.python.org/download/releases/3.0/) 
* [Node.js](https://nodejs.org/en/)
* [Sass](http://sass-lang.com/)

**Note:** Linux and Windows users, please help us make this doc better! 
OS X and Linux have been verified, but Windows is untested. For Linux, our 
best guess is the `sudo apt-get install <item>` incantation, replacing `<item>` 
for each dependency.

### Running DAC locally

Run a live reloading version of the project with two shells open:

* Shell 1: Run this command from the root directory of the repo: 

        hugo server

* Shell 2: Run these commands to compile the javascripts/css:

        cd themes/adg3
        npm run build

**Note:** We recommend changing the `PygmentsCodeFences` value to false in 
`config.toml` if you run into issues with too many files being open:

    PygmentsCodeFences = false