var helper = require('./lib/helper');
var JavaScriptAPI = require('./lib/javascript-api');
var Navigation = require('./lib/navigation');
var Content = require('./lib/content');
var RSVP = require('rsvp');

// URLS
const JAVASCRIPT_DATA_URL = 'https://bitbucket.org/khanhfucius/dac_files/downloads/jsapi.json';
const MODULE_DATA_URL = 'https://bitbucket.org/khanhfucius/dac_files/downloads/connect_modules.json';

let commonModules, jiraModules, confluenceModules, fragments;

const PRODUCT = {
  'jiracloud': {
    path: ['cloud', 'jira', 'platform']
  },
  'jswcloud': {
    path: ['cloud', 'jira', 'software']
  },
  'jsdcloud': {
    path: ['cloud', 'jira', 'service-desk']
  } 
}

function generateConnectModules(name, moduleData, isFragment) {
  var thisModule = new Content(name, moduleData);
  thisModule.highlightCode();

  Navigation.getProducts().forEach(product => {
    try {
      if (!isFragment) {
        Navigation.addItemToSubcategory({
          title: moduleData.title,
          url: `/${PRODUCT[product].path.join('/')}/modules/${name}`
        }, {
          product: product,
          category: 'reference',
          subcategory: 'modules'
        });
      }
      
      thisModule.write(`${PRODUCT[product].path.join('/')}/modules`, {
        platform: 'cloud',
        product: product,
        category: 'reference',
        subcategory: 'modules',
        type: 'connectModule',
        aliases: [
          `/${PRODUCT[product].path.join('/')}/modules/${isFragment ? 'fragment/': ''}${name}.html`
        ]
      });

    } catch (e) {
      console.log(`${product} does not have reference.modules.`);
      return;
    }
  })
}

function parseModuleData(moduleData) {
  var rawModules = JSON.parse(moduleData);
  
  commonModules = rawModules['common'];
  jiraModules = rawModules['jira'];
  confluenceModules = rawModules['confluence'];
  fragments = rawModules['fragment'];

  Object.keys(commonModules).forEach(modName => {  
    generateConnectModules(modName, commonModules[modName], false);
  });

  Object.keys(jiraModules).forEach(modName => {  
    generateConnectModules(modName, jiraModules[modName], false);
  });

  Object.keys(fragments).forEach(modName => {  
    generateConnectModules(modName, fragments[modName], true);
  });

  var fragmentContent = new Content('connect_fragments', fragments);
  fragmentContent.highlightCode();
  fragmentContent.writeData();
}

function generateConnectJSDoc(mod, isClass) {
  if (mod.name) {
      var connectContent = new Content(mod.name, mod);
      connectContent.highlightCode();
      
      Navigation.getProducts().forEach(product => {
        try {
          if (!isClass) {
            Navigation.addItemToSubcategory({
              title: mod.humanizeName,
              url: `/${PRODUCT[product].path.join('/')}/jsapi/${mod.name}`
            }, {
              product: product,
              category: 'reference',
              subcategory: 'jsapi'
            });
          }

          var aliases = [
            `/${PRODUCT[product].path.join('/')}/javascript/${isClass ? `${mod.memberof ? mod.memberof + '~':''}` : 'module-' }${mod.name}.html`,
            `/${PRODUCT[product].path.join('/')}/jsapi/${isClass ? `${mod.memberof ? mod.memberof + '~':''}` : 'module-' }${mod.name}.html`,
            `/${PRODUCT[product].path.join('/')}/javascript/${isClass ? `${mod.memberof ? mod.memberof + '~':''}` : 'module-' }${mod.name}`,
            `/${PRODUCT[product].path.join('/')}/jsapi/${isClass ? `${mod.memberof ? mod.memberof + '~':''}` : 'module-' }${mod.name}`
          ];

          if (isClass) {
            aliases.push(`/${PRODUCT[product].path.join('/')}/jsapi/classes/${mod.memberof ? mod.memberof + '~':''}${mod.name}`)
            aliases.push(`/${PRODUCT[product].path.join('/')}/javascript/classes/${mod.memberof ? mod.memberof + '~':''}${mod.name}`)
          }

          connectContent.write(`${PRODUCT[product].path.join('/')}/jsapi${isClass ? '/classes': ''}`, {
            platform: 'cloud', 
            product: product,
            subcategory: 'jsapi',
            aliases: aliases
          });
        } catch (e) {
          console.log(`${product} does not have reference.jsapi.`);
          return;
        }
      });
    }
}

const downloadPromises =  {
  jsApi: helper.downloadData(JAVASCRIPT_DATA_URL),
  modules: helper.downloadData(MODULE_DATA_URL) 
};


RSVP.hash(downloadPromises).then(data => {
  var jsApi = new JavaScriptAPI(JSON.parse(data.jsApi));

  parseModuleData(data.modules);

  // Generate JavaScript modules docs
  jsApi.modules.forEach(mod => {
    generateConnectJSDoc(mod, false);
  });

  jsApi.classes.forEach(klass => {
    generateConnectJSDoc(klass, true);
  })

  // Update navigation.yml
  Navigation.write();
}).catch(err => {
  console.log(err);
});