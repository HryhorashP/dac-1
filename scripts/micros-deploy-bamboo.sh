#!/bin/bash

set -ex

export MICROS_TOKEN="$bamboo_micros_token_password"
export PATH=`npm bin`:$PATH

./scripts/micros-deploy.sh $@
