---
title: OAuth for REST APIs
platform: cloud
product: jsdcloud
category: devguide
subcategory: security
date: "2016-10-31"
---
{{< reuse-page path="content/cloud/jira/platform/jira-rest-api-oauth-authentication.md">}}