---
title: Conditions
platform: cloud
product: jsdcloud
category: devguide
subcategory: blocks
date: "2016-10-31"
---
{{< include path="content/cloud/connect/concepts/conditions.snippet.md">}}