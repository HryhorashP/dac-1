---
title: Extension points for project configuration
platform: cloud
product: jswcloud
category: reference
subcategory: modules
date: "2016-11-02"
---

{{< reuse-page path="content/cloud/jira/platform/extension-points-for-project-configuration.md">}}