# Conditions

A condition specifies requirements that must be met for a user interface element exposed by an add-on to be displayed
to a user. Typical use cases include requiring a user to be logged in or to have specific permissions.

Various types of modules accept conditions, including
[Pages](../modules/common/page.html), [Web Panels](../modules/common/web-panel.html) and [Web Items](../modules/common/web-item.html).
To see whether a certain module accepts conditions, see its specific module documentation page.

Conditions may also be used in [context parameters](../concepts/context-parameters.html#inline-conditions).

There are different classes of conditions. Most commonly used are [the predefined conditions provided by each host product](#static).
When further customization is needed, conditions can be specified [in terms of properties stored by the add-on in the host product](#entity-property).

These conditions operate on properties of the user at the browser, the entity being viewed (e.g. an issue or a blog post)
or its container (e.g. a project or a space), or the entire system.

Add-ons can also use conditions as building blocks of [boolean expressions](#boolean-operations) containing the
logical operations conjunction (AND), disjunction (OR), or negation.

## Table of contents

* [Predefined conditions](#static)
  * [Condition parameters](#static-condition-parameters)
  * [Property conditions](#property-conditions)
    * [Equal to example](#property-conditions-example)
    * [Contains example](#property-conditions-contains-example)
    * [Aliases](#property-conditions-aliases)
  * [`addon_is_licensed` condition](#addon-is-licensed-condition)
  * [`can_use_application` condition](#can-use-application)
  * [Time tracking conditions](#time-tracking-conditions)
* [Boolean operations](#boolean-operations)
* [Remote conditions (deprecated)](#remote)
* [Appendix: List of predefined conditions](#product-specific-conditions)
  * [Confluence](#confluence-conditions)
  * [JIRA](#jira-conditions)
    * [Condition parameter mappings](#jira-condition-parameters)

## <a name="static"></a>Predefined conditions

A predefined condition is a condition which is exposed from the host Atlassian application.
See [the list of predefined conditions](#product-specific-conditions).

For example, a condition that will evaluate when only logged-in users view the page is specified by the following
module declaration.


``` javascript
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "user_is_logged_in"
                    }
                ]
            }
        ]
    }
}
```

### <a name="static-condition-parameters"></a>Condition parameters

Certain predefined conditions accept parameters.

For example, the `has_issue_permission` condition passes only for users who have the permission specified in the
ondition. The issue for which permissions are checked is the issue being viewed by the user at the browser.

``` javascript
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "has_issue_permission",
                        "params": {
                            "permission": "RESOLVE_ISSUE"
                        }
                    }
                ]
            }
        ]
    }
}
```

### <a name="property-conditions"></a>Property conditions

Add-ons that need to impose custom requirements on when user interface elements are displayed can use a property condition.

The following property conditions exist:

 - `entity_property_equal_to`
 - `entity_property_contains_any` 
 - `entity_property_contains_all` 

These allow fast comparisons to be made against data (properties) stored by the add-on in the host product. See the
[Hosted Data Storage](hosted-data-storage.html) page for more details. Usually, properties are set by a REST call against
an entity type. See the documentation of [the product REST API's](../rest-apis/#product-apis) for information about how to
manage properties for each entity.

Property conditions are defined in the atlassian-connect.json file in a similar way to built in conditions. An example is below.

``` javascript
{
    "modules": {
        "webItems": [
            {
                "key": "conditional-web-item",
                "name": {
                    "value": "Conditional Web Item"
                },
                "url": "/conditional-web-item",
                "location": "page.metadata.banner",
                "conditions": [
                    {
                        "condition": "entity_property_equal_to",
                        "params": {
                            "entity": "content",
                            "propertyKey": "workflow",
                            "value": "In-Progress",
                            "objectName": "state"
                        }
                    }
                ]
            }
        ]
    }
}
```

The condition requires three parameters and can contain (as in the above example) an optional fourth parameter.

##### Required parameters

* `entity` - the entity on which the property has been stored. If an entity of the expected type is not present in the 
  rendering context of the user interface element, the condition evaluates to false.
  * Common: [`addon`](hosted-data-storage.html)
  * Confluence: `content`, `space`
  * JIRA: `project`, `issue`, `user`, `issuetype`, `comment`, `dashboarditem`
* `propertyKey` - the key of the property to check. If the property is not present, the condition evaluates to false.
* `value` - the value to compare the property value against.

##### Optional parameters

The optional parameter `objectName` lets you select which part of the entity property JSON value you wish to compare 
against. For example, if an entity property had the following value:
``` json 
    {
        "one": {
            "ignored": "value",
            "two": true
        },
        "also", "ignored"
    }
```    
Then you could set the `value` parameter to `true` and the `objectName` parameter to `"one.two"` and the condition would 
evaluate to `true`. Specifying an `objectName` string which cannot be used to traverse the property value results in `false`.

#### <a name="property-conditions-example"></a>Property equal to example

An add-on could let administrators activate functionality per JIRA project. This could be achieved by storing an entity 
property with the key `mySettings` and the value `{"isEnabled" : true}` on each project using the product's REST API. 
Then use the `entity_property_equal_to` to test for it with the following module definition:

``` json
{
    "modules": {
        "webPanels": [
            {
                "location": "atl.jira.view.issue.right.context",
                "conditions": [
                    {
                        "condition": "entity_property_equal_to",
                        "params": {
                            "entity": "project",
                            "propertyKey": "mySettings",
                            "objectName": "isEnabled",
                            "value": "true"
                        }
                    }
                ]
            }
        ]
    }
}
```

#### <a name="property-conditions-contains-example"></a>Property contains any example

It is also possible to write conditions where you can check if one or all of the values in the condition are present in the
hosted data.

For example, imagine that you stored the following data against a JIRA project under the property key *consumingTeams*:
``` json
    {
        "teamCount": 4,
        "teams": ["Legal", "Human Resources", "Accounting", "Taxation"]
    }
    
You could then write a condition to only apply to projects that are for the 'Accounting' and 'Development' teams, like so:

    {
        "condition": "entity_property_contains_any",
        "params": {
            "entity": "project",
            "propertyKey": "consumingTeams",
            "objectName": "teams",
            "value": "[\"Accounting\", \"Development\"]"
        }
    }
    
You will note that the `value` parameter needs to be a string escaped JSON element. 

#### <a name="property-conditions-aliases"></a>Aliases

The following aliases can be used which make writing property conditions more convenient.

__Confluence__
	
* `content_property_equal_to`
* `content_property_contains_any`
* `content_property_contains_all`
* `space_property_equal_to`
* `space_property_contains_any`
* `space_property_contains_all`

__Common__

* `addon_property_equal_to`
* `addon_property_contains_any`
* `addon_property_contains_all`

When using these aliases, the `entity` param is not used. This would mean the following two condition definitions are identical.

``` json
{
	"condition": "entity_property_equal_to",
	"params": {
		"entity": "content"
		"propertyKey": "flagged",
		"value": "true"
	}
}
```

``` json
{
	"condition": "content_property_equal_to",
	"params": {
		"propertyKey": "flagged",
		"value": "true"
	}
}
```

### <a name="addon-is-licensed-condition"></a>addon\_is\_licensed condition

The `addon_is_licensed` condition will evaluate to `true` if and only if your add-on is a paid add-on and it 
is licensed. This condition can be placed on any Atlassian Connect module that supports conditions; it is not context sensitive. 
Here is an example of the new condition in use:
``` json
    "jiraIssueTabPanels": [{
        "conditions": [
            {
                "condition": "addon_is_licensed"
            },
            {
                "condition": "user_is_logged_in"
            }
        ], 
        "key": "your-module-key", 
        "name": {
            "value": "Your module name"
        }, 
        "url": "/panel/issue?issue_id={issue.id}&issue_key={issue.key}", 
        "weight": 100
    }]
```    
In this example the JIRA Issue Tab Panel will only be shown if the add-on is licensed and the user is logged in.

<div class="aui-message info">
    <span class="aui-icon icon-info"></span>
    If you give away your add-on for _free_ then you must not use the `addon_is_licensed` condition. This is important because
    all free add-ons are considered _unlicensed_ and will thus the condition will return `false`. Only use this condition with
    paid add-ons that are licensed via the Atlassian Marketplace.
</div>

### <a name="can-use-application"></a>can\_use\_application condition

`can_use_application` condition checks whether the current user is allowed to use a specific application 
 (like JIRA Software or JIRA Service Desk). 
 
 The condition is true if and only if both of the the following statements are true:
 
 * the application is installed and enabled on the JIRA instance
 * the user is permitted to use the application according to the installed licence
 
The condition requires an `applicationKey` parameter, for example:
 
 ``` json
 {
     "modules": {
         "generalPages": [
             {
                 "conditions": [
                     {
                         "condition": "can_use_application",
                         "params": { 
                            "applicationKey": "jira-software"
                         }
                     }
                 ]
             }
         ]
     }
 }
 ```
 
Supported application keys are:

* jira-core
* jira-servicedesk
* jira-software

If an unrecognized application key is provided then the condition will simply evaluate to `false`.

### <a name="time-tracking-conditions"></a>Time tracking conditions

There are three time tracking conditions available:
- `time_tracking_enabled` evaluates to true if time tracking has been turned on
- `jira_tracking_provider_enabled` condition checks whether the currently selected time tracking provider is equal to the JIRA supplied one
- `addon_time_tracking_provider_enabled` condition checks whether the currently selected time tracking provider is equal to the one which is defined in the condition parameters
 
This `addon_time_tracking_provider_enabled` condition can be used with the [`jiraTimeTrackingProviders`](../modules/jira/time-tracking-provider.html) module to check if the add-on provided module is currently selected.
  
For a time tracking provider module coming from an add-on with key `add-on-key` with module key `module-key`: 
``` json
{
  "key": "add-on-key",
  "modules": {
    "jiraTimeTrackingProviders" : [
      {
        "key": "module-key",
        "name": "My time tracking module"
      }
    ]    
  }
}
``` 
 
the condition would be
 
``` json
{
  "condition": "addon_time_tracking_provider_enabled",
  "params": { 
    "addonKey": "add-on-key",
    "moduleKey": "module-key"
  }
}        
```
 
If an unrecognized `addonKey` or `moduleKey` is provided then the condition will simply evaluate to `false`.

## <a name="boolean-operations"></a>Boolean operations

The [Composite Condition](../modules/fragment/composite-condition.html) module fragment can be used wherever a condition
is expected. This allows the construction of boolean expressions aggregating multiple conditions.

For example, a condition that will evaluate when only anonymous users view the page is specified by the following
module declaration.


``` json
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "user_is_logged_in",
                        "invert": false
                    }
                ]
            }
        ]
    }
}
```

## <a name="remote"></a>Remote conditions

As of Atlassian Connect 1.1.96, remote conditions are deprecated and should not be used. Remote conditions will be removed
 from JIRA and Confluence on December 31, 2016. If required, please visit an
 [older version of this document](https://developer.atlassian.com/static/connect/docs/1.1.94/concepts/conditions.html#remote).

## <a name="product-specific-conditions"></a>Appendix: List of predefined conditions

Each product defines a set of conditions relevant to its domain.

### <a name="common-conditions"></a>Common

* [`entity_property_equal_to`](#property-conditions)
* `feature_flag`
* `user_is_admin`
* `user_is_logged_in`
* `user_is_sysadmin`
* `addon_is_licensed`

### <a name="confluence-conditions"></a>Confluence

* `active_theme`
* `can_edit_space_styles`
* `can_signup`
* `content_has_any_permissions_set`
* `content_property_equal_to`
* `create_content`
* `email_address_public`
* `favourite_page`
* `favourite_space`
* `following_target_user`
* `has_attachment`
* `has_blog_post`
* `has_page`
* `has_space`
* `has_template`
* `latest_version`
* `not_personal_space`
* `printable_version`
* `showing_page_attachments`
* `space_function_permission`
* `space_property_equal_to`
* `space_sidebar`
* `target_user_has_personal_blog`
* `target_user_has_personal_space`
* `threaded_comments`
* `tiny_url_supported`
* `user_can_create_personal_space`
* `user_can_use_confluence`
* `user_favouriting_target_user_personal_space`
* `user_has_personal_blog`
* `user_has_personal_space`
* `user_is_confluence_administrator`
* `user_logged_in_editable`
* `user_watching_page`
* `user_watching_space`
* `user_watching_space_for_content_type`
* `viewing_content`
* `viewing_own_profile`

### <a name="jira-conditions"></a>JIRA

* `can_attach_file_to_issue`
* `can_manage_attachments`
* `has_issue_permission`
* `has_project_permission`
* `has_selected_project`
* `has_sub_tasks_available`
* `has_voted_for_issue`
* `is_admin_mode`
* `is_issue_assigned_to_current_user`
* `is_issue_editable`
* `is_issue_reported_by_current_user`
* `is_issue_unresolved`
* `is_sub_task`
* `is_watching_issue`
* `linking_enabled`
* `sub_tasks_enabled`
* [`time_tracking_enabled`](#time-tracking-conditions)
* `user_has_issue_history`
* `user_is_project_admin`
* `user_is_the_logged_in_user`
* `voting_enabled`
* `watching_enabled`
* [`can_use_application`](#can-use-application)
* [`jira_time_tracking_provider_enabled`](#time-tracking-conditions)
* [`addon_time_tracking_provider_enabled`](#time-tracking-conditions)

##### <a name="jira-servicedesk-conditions"></a>JIRA Service Desk
* `servicedesk.is_agent`

    True if user is an agent for a particular Service Desk project
* `servicedesk.is_customer`

    True if user raised or is a request participant for a particular Service Desk issue

#### <a name="jira-condition-parameters"></a>JIRA permission keys


`has_issue_permission`, `has_project_permission` and `has_global_permission` require a key of the permission that 
will be checked for the current user. The first two conditions check project permissions and the last one checks global permissions. 
Below you will find all the built-in permission keys. 

Note that you may also provide any of your custom permission keys 
(defined in the [project](../modules/jira/project-permission.html) or [global](../modules/jira/global-permission.html) permission module). 
Permissions defined by add-ons need to be prefixed with an add-on key followed by two underscores and only then the custom permission key, 
for example: `your.add.on.key__yourPermissionKey`.

##### Project permission keys

* ADD_COMMENTS
* ADMINISTER_PROJECTS
* ASSIGN_ISSUES
* ASSIGNABLE_USER
* BROWSE_PROJECTS
* CLOSE_ISSUES
* CREATE_ATTACHMENTS
* CREATE_ISSUES
* DELETE_ALL_ATTACHMENTS
* DELETE_ALL_COMMENTS
* DELETE_ALL_WORKLOGS
* DELETE_ISSUES
* DELETE_OWN_ATTACHMENTS
* DELETE_OWN_COMMENTS
* DELETE_OWN_WORKLOGS
* EDIT_ALL_COMMENTS
* EDIT_ALL_WORKLOGS
* EDIT_ISSUES
* EDIT_OWN_COMMENTS
* EDIT_OWN_WORKLOGS
* LINK_ISSUES
* MANAGE_WATCHERS
* MODIFY_REPORTER
* MOVE_ISSUES
* RESOLVE_ISSUES
* SCHEDULE_ISSUES
* SET_ISSUE_SECURITY
* TRANSITION_ISSUES
* VIEW_DEV_TOOLS
* VIEW_READONLY_WORKFLOW
* VIEW_VOTERS_AND_WATCHERS
* WORK_ON_ISSUES


##### Global permission keys

* ADMINISTER
* SYSTEM_ADMIN
* USER_PICKER
* CREATE_SHARED_OBJECTS
* MANAGE_GROUP_FILTER_SUBSCRIPTIONS
* BULK_CHANGE
