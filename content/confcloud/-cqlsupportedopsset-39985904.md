---
title: Cqlsupportedopsset 39985904
aliases:
    - /confcloud/-cqlsupportedopsset-39985904.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985904
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985904
confluence_id: 39985904
platform:
product:
category:
subcategory:
---
# Confluence Connect : \_CQLSupportedOpsSet

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>



