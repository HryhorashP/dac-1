---
title: Cqllistuserfields 39985896
aliases:
    - /confcloud/-cqllistuserfields-39985896.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985896
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985896
confluence_id: 39985896
platform:
product:
category:
subcategory:
---
# Confluence Connect : \_CQLListUserFields

-   [Creator]
-   [Contributor]
-   [Mention]
-   [Watcher]
-   [Favourite]

  [Creator]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-creatorcreatorCreator
  [Contributor]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-contributorcontributorContributor
  [Mention]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-mentionMentionMention
  [Watcher]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-Watcher
  [Favourite]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-favouriteFavouriteFavourite,favorite

