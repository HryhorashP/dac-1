---
title: Enhancing Page Metadata 39988127
aliases:
    - /confcloud/enhancing-page-metadata-39988127.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988127
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988127
confluence_id: 39988127
platform:
product:
category:
subcategory:
---
# Confluence Connect : Enhancing page metadata

## Graphic elements required

-   16x16 brand or add-on icon to be displayed in the byline along with the text.

## How do users find metadata?

The byline extension lets add-ons provide users with further information about a page's metadata.

## How should the interaction work?

-   Users can find basic metadata in the byline, below the page title.
-   If they then click on the add-on icon or insight text, an inline dialog will show further information or actions related to the add-on.
-   The **View more... **button lets** **add-ons take the user to a dedicated view to see additional details or complete related actions.

## UI components

[Web items], inline dialogs, and modal dialogs can be used to build byline extensions

## ![]

##  Recommendation

-   -   Use progressive disclosure in surfacing the metadata. 

  [Web items]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/web-item.html
  []: /confcloud/attachments/39988127/39988109.png

