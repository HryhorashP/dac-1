---
title: Tutorials 39984583
aliases:
    - /confcloud/tutorials-39984583.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984583
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984583
confluence_id: 39984583
platform:
product:
category:
subcategory:
---
# Confluence Connect : Tutorials

We're creating more tutorials for Confluence Connect, and will add them here as soon as they're ready. Check this page regularly to try out the latest tutorials as we post them.

 

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<thead>
<tr class="header">
<th>Title</th>
<th>Description</th>
<th>Estimated Time</th>
<th>Level</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="/display/CONFCLOUD/Content+Byline+Items+with+Confluence+Connect">Content Byline Items with Confluence Connect</a></td>
<td>A step-by-step introduction to building a Content Byline item in Confluence</td>
<td>25 minutes</td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
2 - BEGINNER
</div></td>
</tr>
<tr class="even">
<td><a href="/display/CONFCLOUD/Quick+start+to+Confluence+Connect">Quick start to Confluence Connect</a></td>
<td>An accelerated guide to building a basic &quot;hello world&quot; macro using Confluence Connect.</td>
<td>10 minutes</td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
1 - QUICK START
</div></td>
</tr>
<tr class="odd">
<td><a href="/display/CONFCLOUD/Development+setup">Development setup</a></td>
<td><p>Before undertaking any of our tutorials, this will help you setup your development environment.</p></td>
<td>15 minutes</td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
0 - SETUP
</div></td>
</tr>
<tr class="even">
<td><a href="/display/CONFCLOUD/Introduction+to+Confluence+Connect">Introduction to Confluence Connect</a></td>
<td>A step-by-step introduction to building a basic Confluence add-on and macro.</td>
<td>45 minutes</td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
2 - BEGINNER
</div></td>
</tr>
<tr class="odd">
<td><a href="/display/CONFCLOUD/Macro+Autoconvert+with+Confluence+Connect">Macro Autoconvert with Confluence Connect</a></td>
<td>Add autoconvert patterns to your macro experience.</td>
<td>15 minutes</td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="even">
<td><a href="/display/CONFCLOUD/Custom+Content+with+Confluence+Connect">Custom Content with Confluence Connect</a></td>
<td>An advanced walkthrough of Custom content in Confluence.</td>
<td>60 minutes</td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="odd">
<td><a href="/display/CONFCLOUD/Macro+Custom+Editor+with+Confluence+Connect">Macro Custom Editor with Confluence Connect</a></td>
<td>Add a custom editor to your macro experience.</td>
<td>15 minutes</td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="even">
<td><a href="/display/CONFCLOUD/Multi-page+Blueprints+with+Confluence+Connect">Multi-page Blueprints with Confluence Connect</a></td>
<td><p>A quick guide to building a multi-page blueprint with a custom parent and child pages.</p></td>
<td><p>1 hour</p></td>
<td><p>3 - INTERMEDIATE</p></td>
</tr>
</tbody>
</table>

  [Content Byline Items with Confluence Connect]: /display/CONFCLOUD/Content+Byline+Items+with+Confluence+Connect
  [Quick start to Confluence Connect]: /display/CONFCLOUD/Quick+start+to+Confluence+Connect
  [Development setup]: /display/CONFCLOUD/Development+setup
  [Introduction to Confluence Connect]: /display/CONFCLOUD/Introduction+to+Confluence+Connect
  [Macro Autoconvert with Confluence Connect]: /display/CONFCLOUD/Macro+Autoconvert+with+Confluence+Connect
  [Custom Content with Confluence Connect]: /display/CONFCLOUD/Custom+Content+with+Confluence+Connect
  [Macro Custom Editor with Confluence Connect]: /display/CONFCLOUD/Macro+Custom+Editor+with+Confluence+Connect
  [Multi-page Blueprints with Confluence Connect]: /display/CONFCLOUD/Multi-page+Blueprints+with+Confluence+Connect

