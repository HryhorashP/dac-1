---
title: Development Setup 39988911
aliases:
    - /confcloud/development-setup-39988911.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988911
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988911
confluence_id: 39988911
platform:
product:
category:
subcategory:
---
# Confluence Connect : Development setup

 

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td><p>Before undertaking any of our tutorials, this will help you setup your development environment.</p></td>
</tr>
<tr class="even">
<td>Level</td>
<td><div class="content-wrapper">
0 - SETUP
</div></td>
</tr>
<tr class="odd">
<td>Estimated Time</td>
<td>15 minutes</td>
</tr>
<tr class="even">
<td>Example</td>
<td>N/A</td>
</tr>
</tbody>
</table>

**If you're just starting out with Confluence and Connect, then this is the place for you.**

This tutorial will get you setup and ready to develop Connect add-ons for Confluence using the Atlassian Connect Express stack (ACE) which is based on NodeJS Express.

**I want to use Java not Node**

You can still follow along with this guide to get to the same end point. If you are interested in Spring Boot check out <https://developer.atlassian.com/blog/2016/03/connecting-connect-with-spring-boot/>.

**I want to use Ruby/PHP/Python/etc.**

Check the [Atlassian Connect Documentation] to see which frameworks are available.

To begin developing with Confluence Connect, ensure you have the following tools installed:

-   <a href="https://git-scm.com/downloads" class="external-link">git</a>
-   <a href="https://nodejs.org/en/download/" class="external-link">node.js</a>
-   npm (installed with node)
-   <a href="https://www.npmjs.com/package/atlas-connect" class="external-link">atlas-connect</a>

This covers all that we need to run our Connect add-on. You will also need a development version of Confluence up as per <a href="http://go.atlassian.com/about-cloud-dev-instance" class="uri" class="external-link">http://go.atlassian.com/about-cloud-dev-instance</a>. A more concrete walkthrough is provided below.

# What is ACE?

Welcome to Confluence Connect! We're looking forward to showing you how to harness the power of Connect and ACE to build amazing add-ons for our products. To begin with, let's take a quick look at **A**tlassian **C**onnect **E**xpress. ACE is a NodeJS toolkit for building Atlassian Connect add-ons. As the name suggests, it utilises the Express framework, allowing you to leverage routing, middleware, and templating, within a Node environment. 

# ACE Prerequisites

In order to start using ACE, the following packages must be installed on your system:

-   homebrew (OSX only)
-   NodeJS
-   npm
-   atlas-connect

Let's walk through how to get these really quickly.

 

**OSX/Linux**

Run the following command in your terminal:

``` javascript
my-awesome-machine $ > /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

 

 

 

If you have **homebrew** installed, issue the following command:

``` javascript
my-awesome-machine $ > brew install node
```

This will install both node, and npm on your system.

 

 

 

**Windows**

Use the installer available from <a href="https://nodejs.org/en/download/" class="external-link">here</a>.

This will also ensure npm is installed on your system.

# An ACE up your sleeve

Awesome, now we're ready to get ACE! 

Before moving on, check that you have node and npm setup correctly.

``` javascript
my-awesome-machine $ > node -v
v5.5.0
my-awesome-machine $ > npm -v
3.5.3
```

 

If either of the above commands aren't recognised, please refer to the previous installation steps. To install atlas-connect, issue the following command:

``` javascript
my-awesome-machine $ > npm install -g atlas-connect
```

 

Verify it is installed by running:

``` javascript
my-awesome-machine $ > atlas-connect --help
```

 

You should see a helpful set of text like the following:

``` javascript
  Usage: atlas-connect [options] <command>


  Commands:

    new [options] <AwesomeApp>  Creates a new project with the given AwesomeApp name

  Options:

    -h, --help  output usage information
```

# Getting Confluence

Follow the steps provided by the official Atlassian Connect Documentation at <a href="http://go.atlassian.com/about-cloud-dev-instance" class="uri" class="external-link">http://go.atlassian.com/about-cloud-dev-instance</a>, to get an official Confluence Cloud development instance.

  [Atlassian Connect Documentation]: https://developer.atlassian.com/static/connect/docs/latest/developing/frameworks-and-tools.html

