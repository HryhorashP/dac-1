---
title: Custom Content with Confluence Connect 40511954
aliases:
    - /confcloud/custom-content-with-confluence-connect-40511954.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40511954
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40511954
confluence_id: 40511954
platform:
product:
category:
subcategory:
---
# Confluence Connect : Custom Content with Confluence Connect

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>An advanced walkthrough of Custom content in Confluence.</td>
</tr>
<tr class="even">
<td>Level</td>
<td><div class="content-wrapper">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="odd">
<td>Estimated Time</td>
<td>60 minutes</td>
</tr>
</tbody>
</table>

# Prerequisites

Ensure you have installed all the tools you need for Confluence Connect add-on development, and running Confluence by going through the [Development Setup].

# Lessons

In this series, we build a content application which exposes two new Content Types to Confluence - Customers and Notes, where Notes are contained and stored under Customers. 

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<thead>
<tr class="header">
<th>Title</th>
<th>Description</th>
<th>Estimated Time</th>
<th>Example</th>
<th>Level</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="/display/CONFCLOUD/Lesson+1+-+A+New+Content+Type">Lesson 1 - A New Content Type</a></td>
<td>Setup a new Customer content type</td>
<td>10 minutes</td>
<td><a href="https://bitbucket.org/atlassianlabs/confluence-custom-content-example" class="uri" class="external-link">https://bitbucket.org/atlassianlabs/confluence-custom-content-example</a></td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="even">
<td><a href="/display/CONFCLOUD/Lesson+2+-+Adding+Content+-+Customers+Ahoy">Lesson 2 - Adding Content - Customers Ahoy</a></td>
<td>Setup and create instances of a new Customer content type.</td>
<td>15 minutes</td>
<td><a href="https://bitbucket.org/atlassianlabs/confluence-custom-content-example" class="uri" class="external-link">https://bitbucket.org/atlassianlabs/confluence-custom-content-example</a></td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="odd">
<td><a href="/display/CONFCLOUD/Lesson+3+-+Extra+Searching+Capabilities">Lesson 3 - Extra Searching Capabilities</a></td>
<td>Add extra searching capabilities to our Customer content type</td>
<td>15 minutes</td>
<td><a href="https://bitbucket.org/atlassianlabs/confluence-custom-content-example" class="uri" class="external-link">https://bitbucket.org/atlassianlabs/confluence-custom-content-example</a></td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="even">
<td><a href="/display/CONFCLOUD/Lesson+4+-+Noting+Down+Information">Lesson 4 - Noting Down Information</a></td>
<td>Setup and create instances of a new Note content type, contained within 'Customers'.</td>
<td>20 minutes</td>
<td><a href="https://bitbucket.org/atlassianlabs/confluence-custom-content-example" class="uri" class="external-link">https://bitbucket.org/atlassianlabs/confluence-custom-content-example</a></td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
</tbody>
</table>

# Further reading

-   <a href="http://connect.atlassian.com/" class="external-link">Atlassian Connect Documentation</a>

  [Development Setup]: https://developer.atlassian.com/confcloud/development-setup-39988911.html
  [Lesson 1 - A New Content Type]: /display/CONFCLOUD/Lesson+1+-+A+New+Content+Type
  [Lesson 2 - Adding Content - Customers Ahoy]: /display/CONFCLOUD/Lesson+2+-+Adding+Content+-+Customers+Ahoy
  [Lesson 3 - Extra Searching Capabilities]: /display/CONFCLOUD/Lesson+3+-+Extra+Searching+Capabilities
  [Lesson 4 - Noting Down Information]: /display/CONFCLOUD/Lesson+4+-+Noting+Down+Information

