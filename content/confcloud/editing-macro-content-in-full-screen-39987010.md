---
title: Editing Macro Content in Full Screen 39987010
aliases:
    - /confcloud/editing-macro-content-in-full-screen-39987010.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987010
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987010
confluence_id: 39987010
platform:
product:
category:
subcategory:
---
# Confluence Connect : Editing macro content in full screen

If your macro integrates an existing SaaS product with a fully-fledged user interface, you can use the full screen dialog to bring your product interface into Confluence. The chrome on the full screen dialog appears as a black header, and maintains the context for the user of being inside Confluence. It also ensures users have a consistent path back to the main Confluence UI.

.

<img src="https://pug.jira-dev.com/wiki/download/attachments/1791459347/immersive_mode.gif?version=1&amp;modificationDate=1463038535847&amp;api=v2" class="confluence-external-resource" width="792" />

## UI components

*Full screen dialog*

<img src="/confcloud/attachments/39987010/39987273.png" width="900" />

 

## Recommendations 

**Do:** 

-   Design an 'Open in' action to allow the user to access the full screen dialog.
-   Use "Back" or "Back to Confluence" to allow users to return to Confluence.
-   Follow a design style involving white outline and a pixel width stroke. 

**Don't:** 

-   Use labels like "Exit", "Close", "Save and Close", as they may confuse users and conflict with parts of the Confluence UI.
-   Provide on-boarding for users during editing. If you need to provide on-boarding, do it when users are inserting your macro.

*
*



