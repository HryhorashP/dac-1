---
title: Confluence Connect Development 39375836
aliases:
    - /confcloud/confluence-connect-development-39375836.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39375836
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39375836
confluence_id: 39375836
platform:
product:
category:
subcategory:
---
# Confluence Connect : Confluence Connect Development

# Confluence Connect

##### Integrate with or extend Confluence Cloud for millions of content collaborators around the world

##### Join the rapidly-growing list of vendors in the Atlassian Marketplace

.

Start now with a 10 minute add-on
 

------------------------------------------------------------------------

 

<img src="/confcloud/attachments/39375836/39984493.png" class="image-center" />

## Getting started

Get started quickly with some context and our 5 minute add-on tutorial.

[What can I build?]

[10 minute add-on tutorial]

[What is Confluence Connect?]

<img src="/confcloud/attachments/39375836/39984495.png" class="image-center" />

## Guides and tutorials

Learn how everything in Confluence Connect works and how to build your add-on.

[Security]

[Patterns and principles]

[Tutorials]

<img src="/confcloud/attachments/39375836/39984496.png" class="image-center" />

## Code reference

Look up specifics in technical references for extension points and APIs.

[Connect Reference (Confluence and JIRA)]

<a href="https://docs.atlassian.com/atlassian-confluence/REST/latest/" class="external-link">REST API Reference</a>

[REST API Examples]

.

.

Developing for Confluence Server? Visit the [Confluence Server developer docs].

  [What can I build?]: /confcloud/39984596.html
  [10 minute add-on tutorial]: /confcloud/quick-start-to-confluence-connect-39987884.html
  [What is Confluence Connect?]: /confcloud/39985284.html
  [Security]: https://developer.atlassian.com/static/connect/docs/latest/concepts/security.html
  [Patterns and principles]: /confcloud/confluence-connect-patterns-39981569.html
  [Tutorials]: /confcloud/tutorials-39984583.html
  [Connect Reference (Confluence and JIRA)]: https://developer.atlassian.com/connect
  [REST API Examples]: /confcloud/confluence-rest-api-39985291.html
  [Confluence Server developer docs]: https://developer.atlassian.com/display/CONFDEV

