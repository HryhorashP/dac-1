---
title: "Atlassian introduces new cloud development environment"
date: "2016-04-28T12:00:00+07:00"
author: "rmassaioli"
categories: ["atlassian-connect", "amps"]
lede: "The Atlassian Connect team is proud to announce the launch of the cloud development environment for JIRA and
       Confluence. We are really excited about this new environment and want to share with you a little more info about 
       how to use it to build your add-ons."
---

The Atlassian Connect team is proud to announce the launch of the cloud development environment for JIRA and
Confluence. We are really excited about this new environment and want to share with you a little more info about 
how to use it to build your add-ons.
 
## Why we did this
 
Until now, you had to use the Atlassian Plugin SDK to run a "local cloud" version of JIRA or Confluence.
This isn't a great experience, and running a "local cloud" version comes with a number of caveats:
 
 - Permissions on a local development environment are subtly different from cloud
 - Local development licenses expire every few days
 - Performance of a local development instance is different from cloud instances
 - You have to download hundreds of megabytes of dependencies to run JIRA and Confluence locally
 - Starting "local cloud" versions of the products takes time and you will likely start the the "local cloud" environment more than once
 
The caveats above mean that you are developing against an environment that is different from production, and
every difference between development and production is a difference that could blossom into a bug &mdash; the
fewer differences the better.
 
## Cloud development instances
 
In order to make the cloud development experience better, we have designed a new environment for you that includes
[free development instances](http://go.atlassian.com/about-cloud-dev-instance) exclusively for add-on developers.
 
When you sign up for a developer instance, you will get:
 
 - JIRA Core: 5 users free
 - JIRA Software: 5 users free
 - Confluence: 5 users free
 - JIRA Service Desk: 1 agent free
 
We have also added *Development mode* to the settings avaliable on the *Manage add-ons* page. _Development mode_ enables the installation of 
Atlassian Connect descriptors that do not come from the Atlassian Marketplace. This means you can install your development 
add-ons directly in your instance without creating a Marketplace listing.
 
<div style="text-align:center;margin:20px;"><a href="http://go.atlassian.com/about-cloud-dev-instance" class="aui-button aui-button-primary"><span class="aui-icon aui-icon-small aui-iconfont-deploy">Deployment</span> Get your development environment</a> </div>

## Local development
 
You will continue to develop add-ons in your local development environment. The main difference is that you'll now be
[creating a local proxy](http://go.atlassian.com/ac-local-proxy) that enables you to install your add-on in your cloud instance.
 
## Retirement of the Atlassian Plugin SDK for cloud development
 
We are no longer supporting the use of the Atlassian Plugin SDK for local development of cloud add-ons. Since the
vagrant image depends on the Atlassian Plugin SDK it too is no longer supported.
This means that we will no longer be updating the Atlassian Plugin SDK with new features and bug fixes for cloud development.
Don't worry, the Atlassian Plugin SDK is still supported for server plugin development (P2 plugins).
 
We hope that you are as excited about the new environment as we are and enjoy developing directly against
cloud as much as we do. If you have any questions, feedback, or suggestions please don't hesitate to contact
us on the [Atlassian Connect developers mailing list](https://groups.google.com/forum/#!forum/atlassian-connect-dev)
or on [Atlassian Answers](https://answers.atlassian.com/questions/topics/754005/atlassian-connect?&filter=recent) 
using the label **atlassian-connect**.
