---
title: "Tip of the Week - Triggering a Bamboo build from Bitbucket"
date: "2015-10-08T16:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","Bamboo","Bitbucket"]
lede: "This week's article will explain how you can trigger a build in Bamboo by
committing changes in your Bitbucket repository. It's easy to do and will make
your CI experience so much smoother."
---
Welcome to another Tip of the Week, today we are going to take a look at how you
can integrate Bitbucket and Bamboo in such a way that a commit in Bitbucket will
trigger an automatic build in Bamboo.

First we need to create our Bitbucket repository as a
<a href="https://confluence.atlassian.com/bamboo/linking-to-source-code-repositories-671089223.html">
 Linked repository in Bamboo</a>.

![Linked repository screen in Bamboo](linked-repositories.png "Linked repositories in Bamboo.")

Then we'll need to <a href="https://confluence.atlassian.com/bamboo/creating-a-plan-289276868.html">
create a build plan</a> for the code in this repository.

![Bamboo build plan](build-plan.png "Our Bamboo build plan.")

We must take special care to create the right trigger:

![Configure the correct trigger](trigger.png "Configuring the correct trigger.")

Next up we need to go to our Bitbucket repository and create a
<a href="https://confluence.atlassian.com/display/BITBUCKET/Bamboo+service+management">
Bamboo service</a>.

![Bamboo service in Bitbucket](bamboo-service.png "Configure a Bamboo service in Bitbucket").

That's it, if you now push your changes to Bitbucket the Bamboo build will be triggered automatically.

If you like this article don't hesitate to share it!
