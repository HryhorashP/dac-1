---
title: "The secret life of ngrok"
date: "2015-05-07T16:00:00+07:00"
author: "tsmith"
categories: ["tools"]
---

[ngrok] is a handy tool and service that allows you tunnel requests from the wide 
open Internet to your local machine when it's behind a NAT or firewall. This 
is useful in a number of cases, such as when you want to test out an add-on 
you've been writing for [HipChat] or a custom webhook endpoint for [Bitbucket],
but you haven't yet deployed your code to an Internet accessible host or PaaS. 
The most common usage of ngrok sets up a tunnel to localhost using the random hostname 
ngrok provides by default, e.g., 5a3e3614.ngrok.com. But that's not all it can do...


## Some ngrok secrets

That works reasonably well, but perhaps you want to be able to reuse a 
hostname or use a specific hostname for the tunnel. As it turns out ngrok 
supports [specifying the subdomain used][ngrok-subdomain], giving you a 
hostname of choice. This does require you to sign up for a ngrok account. 
It's free, so I'd suggest [signing up for ngrok][ngrok-signup] to access 
this benefit plus the ability to password protect the tunnel, do 
non-http/https tunnels, and launch multiple tunnels at once. Awesome!

ngrok also tracks requests through the tunnel. They are displayed in the 
console window you used to launch ngrok, but more importantly they can be seen
if you visit the web console ngrok launches at [http://127.0.0.1:4040/]. 

![The ngrok web console](ngrok-console.png)

This console allows you to inspect the latest requests, look at the request (headers
and content), the response (headers and content), and lets you replay requests.
This is super helpful when you have API requests you are testing out and don't
want to bother doing whatever steps it takes to generate that request in your
UI. 

There's also a [client REST API] so you can query information about the ngrok tunnels
active from your local machine. Need to set your app's hostname? Hit the API to 
ask for the external hostname. 

By default ngrok forwards both http and https traffic to your local machine.
This is great when testing out webhooks you're building with services that require 
SSL and a valid, properly signed cert. 

## ngrok should be in your toolkit

This just touches the surface of the cool things you can do with ngrok. ngrok's 
free tier is powerful enough for most use cases that I've come across in every 
day development. There's also paid tiers for people teams that add custom domain names
and reserved hostnames among other things. So keep ngrok in mind next time you're 
working on an add-on for [HipChat][hipchat-addon] or one of our other [cloud products].

Follow me [@TravisTheTechie] on Twitter for more things about the web, LEGOs, and 
tech.

[ngrok]: https://ngrok.com/
[HipChat]: https://www.hipchat.com/
[ngrok-subdomain]: https://ngrok.com/docs#subdomain
[ngrok-signup]: https://dashboard.ngrok.com/user/signup
[http://127.0.0.1:4040/]: http://127.0.0.1:4040/
[client REST API]: https://ngrok.com/docs#client-api
[@TravisTheTechie]: https://twitter.com/travisthetechie
[hipchat-addon]: https://www.hipchat.com/docs/apiv2/addons
[cloud products]: https://developer.atlassian.com/index.html
[Bitbucket]: https://confluence.atlassian.com/display/BITBUCKET/Manage+Bitbucket+hooks