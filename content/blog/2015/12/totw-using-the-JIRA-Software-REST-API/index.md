---
title: "Tip of the Week - Using the JIRA Software REST API"
date: "2015-12-07T16:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","JIRA Software"]
lede: "JIRA Software has a REST API that enables you to interact with your projects,
 issues, sprints, backlogs and much more straight from the command line.
 Or from your favourite programming language."
---
This week's article is about the JIRA Software REST API and some of the things you can do with it.
The full documentation can be found on the [JIRA Software REST API](https://developer.atlassian.com/jiradev/jira-apis?utm_source=dac&utm_medium=blog&utm_campaign=totw) page.

I'll show you the following basic JIRA Software scenario:

1. Creating a project.
2. Adding issues to our project.
3. Creating a first sprint.
4. Adding issues to our first sprint.
5. Starting our first sprint.
6. Moving issues through the workflow.
7. Finishing our first sprint.

*Warning: To use the JIRA Software REST API for anything other than reading information you'll need to be authenticated, even if you can browse the project anonymously!*

For more information about authenticating using the REST API take a look in our
[documentation](https://developer.atlassian.com/jiradev/jira-apis/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-basic-authentication?utm_source=dac&utm_medium=blog&utm_campaign=totw).

1. Creating a project
---------------------
Let's start by creating a project called 'REST API Example Project'.

    curl -u admin -H "Content-Type: application/json" -X POST -d '{
        "key": "REST",
        "name": "REST API Example Project",
        "projectTypeKey": "Software",
        "projectTemplateKey": "com.pyxis.greenhopper.jira:gh-scrum-template",
        "description": "REST API Example Project description",
        "lead": "admin",
        "url": "https://developer.atlassian.com",
        "assigneeType": "PROJECT_LEAD"
    }' https://tip-of-the-week.atlassian.net.example.com/rest/api/2/project | python -m json.tool

As you can see we are piping the JSON to json.tool to get nicely formatted JSON.
I explained this usage in another Tip of the Week: [Making JSON readable on the command-line](https://developer.atlassian.com/blog/2015/11/totw-making-JSON-readable-on-the-command-line?utm_source=dac&utm_medium=blog&utm_campaign=totw).

We get the following JSON reply:

    {
     "self":"https://tip-of-the-week.atlassian.net.example.com/rest/api/2/project/10000",
     "id":10000,
     "key":"REST"
    }

Remember the project id, we'll need it for our next steps.
And here you can see our project in JIRA Software:

![REST API Project in JIRA Software](Project-Created.png "Our created REST API Project in JIRA Software.")

2. Adding issues to our project
-------------------------------
Let's add 3 issues for this example:

    curl -u admin -H "Content-Type: application/json" -X POST -d '{
        "issueUpdates": [
            {
                "fields": {
                    "project": {
                        "id": "10000"
                    },
                    "summary": "REST API Example Task 1",
                    "issuetype": {
                        "id": "10002"
                    },
                    "assignee": {
                        "name": "admin"
                    },
                    "reporter": {
                        "name": "admin"
                    },
                    "priority": {
                        "id": "1"
                    },
                    "description": "This is an example task created through the REST API."
                }
            },
            {
                "fields": {
                    "project": {
                        "id": "10000"
                    },
                    "summary": "REST API Example Task 2",
                    "issuetype": {
                        "id": "10002"
                    },
                    "assignee": {
                        "name": "admin"
                    },
                    "reporter": {
                        "name": "admin"
                    },
                    "priority": {
                        "id": "2"
                    },
                    "description": "This is an example task created through the REST API."
                }
            },
            {
                "fields": {
                    "project": {
                        "id": "10000"
                    },
                    "summary": "REST API Example Task 3",
                    "issuetype": {
                        "id": "10002"
                    },
                    "assignee": {
                        "name": "admin"
                    },
                    "reporter": {
                        "name": "admin"
                    },
                    "priority": {
                        "id": "3"
                    },
                    "description": "This is an example task created through the REST API."
                }
            }
        ]
    }' https://tip-of-the-week.atlassian.net.example.com/rest/api/2/issue/bulk | python -m json.tool

And JIRA Software in combination with json.tool returns us this nice JSON:

    {
     "errors": [],
     "issues": [
         {
             "id": "10000",
             "key": "REST-1",
             "self": "https://tip-of-the-week.atlassian.net.example.com/rest/api/2/issue/10000"
         },
         {
             "id": "10001",
             "key": "REST-2",
             "self": "https://tip-of-the-week.atlassian.net.example.com/rest/api/2/issue/10001"
         },
         {
             "id": "10002",
             "key": "REST-3",
             "self": "https://tip-of-the-week.atlassian.net.example.com/rest/api/2/issue/10002"
         }
         ]
     }

And here we can see those 3 issues in our product backlog in JIRA Software:

![Show just created issues in product backlog.](Issues-Created.png "Our newly created issues in the product backlog.")

3. Creating a first sprint
--------------------------
Now we'll create a first sprint:

    curl -u admin -H "Content-Type: application/json" -X POST -d '{
        "name": "Tip of the Week Sprint 1",
        "startDate": "2015-12-04T15:22:00.000+10:00",
        "endDate": "2015-12-11T01:22:00.000+10:00",
        "originBoardId": 1
    }' https://tip-of-the-week.atlassian.net.example.com/rest/agile/1.0/sprint | python -m json.tool

And the following JSON is returned:

    {
       "endDate": "2015-12-10T16:22:00.000+01:00",
       "id": 1,
       "name": "Tip of the Week Sprint 1",
       "originBoardId": 1,
       "self": "https://tip-of-the-week.atlassian.net.example.com/rest/agile/1.0/sprint/1",
       "startDate": "2015-12-04T06:22:00.000+01:00",
       "state": "future"
    }
Remember the id, we'll need to use this in the following steps.

Our sprint has been nicely created on our Scrum board:

![Show newly created Sprint.](Sprint-Created.png "Our newly created sprint visible in the product backlog.")

4. Adding issues to our first sprint
------------------------------------
 Let's add our 3 issues to this sprint:

    curl -u admin -H "Content-Type: application/json" -X POST -d '{
      "issues": [
         "REST-1",
         "REST-2",
         "REST-3"
     ]
    }' https://tip-of-the-week.atlassian.net.example.com/rest/agile/1.0/sprint/1/issue

And here you can see the result:

![Our issues are now part of our sprint.](Issues-in-Sprint.png "Our issues are now part of our sprint.")

5. Starting our first sprint
----------------------------
Let's get this sprint started!

    curl -u admin -H "Content-Type: application/json" -X POST -d '{
      "state":"active"
    }' https://tip-of-the-week.atlassian.net.example.com/rest/agile/1.0/sprint/1 | python -m json.tool

 We get all the available information back in JSON:

    {
      "endDate": "2015-12-10T16:22:00.000+01:00",
      "id": 1,
      "name": "Tip of the Week Sprint 1",
      "originBoardId": 1,
      "self": "https://tip-of-the-week.atlassian.net.example.com/rest/agile/1.0/sprint/1",
      "startDate": "2015-12-04T06:22:00.000+01:00",
      "state": "active"
    }

 And our sprint has now officially been started:

 ![Our sprint has started!](Sprint-Started.png "Our sprint has started!")

6. Moving issues through the workflow
-------------------------------------
Now we can move our issues through the different steps of our workflow.

We'll first need to know the available transitions, REST to the rescue!

    curl -u admin -H "Content-Type: application/json" -X GET
    https://tip-of-the-week.atlassian.net.example.com/rest/api/2/issue/REST-1/transitions | python -m json.tool

This gives us the following available transitions:

    {
    "expand": "transitions",
    "transitions": [
        {
            "id": "11",
            "name": "To Do",
            "to": {
                "description": "",
                "iconUrl": "https://tip-of-the-week.atlassian.net.example.com/",
                "id": "10000",
                "name": "To Do",
                "self": "https://tip-of-the-week.atlassian.net.example.com/rest/api/2/status/10000",
                "statusCategory": {
                    "colorName": "blue-gray",
                    "id": 2,
                    "key": "new",
                    "name": "To Do",
                    "self": "https://tip-of-the-week.atlassian.net.example.com/rest/api/2/statuscategory/2"
                }
            }
        },
        {
            "id": "21",
            "name": "In Progress",
            "to": {
                "description": "This issue is being actively worked on at the moment by the assignee.",
                "iconUrl": "https://tip-of-the-week.atlassian.net.example.com/images/icons/statuses/inprogress.png",
                "id": "3",
                "name": "In Progress",
                "self": "https://tip-of-the-week.atlassian.net.example.com/rest/api/2/status/3",
                "statusCategory": {
                    "colorName": "yellow",
                    "id": 4,
                    "key": "indeterminate",
                    "name": "In Progress",
                    "self": "https://tip-of-the-week.atlassian.net.example.com/rest/api/2/statuscategory/4"
                }
            }
        },
        {
            "id": "31",
            "name": "Done",
            "to": {
                "description": "",
                "iconUrl": "https://tip-of-the-week.atlassian.net.example.com/",
                "id": "10001",
                "name": "Done",
                "self": "https://tip-of-the-week.atlassian.net.example.com/rest/api/2/status/10001",
                "statusCategory": {
                    "colorName": "green",
                    "id": 3,
                    "key": "done",
                    "name": "Done",
                    "self": "https://tip-of-the-week.atlassian.net.example.com/rest/api/2/statuscategory/3"
                }
            }
        }
    ]
    }

So let's move 2 issues into **Done** and 1 into **In Progress**.

    curl -u admin -H "Content-Type: application/json" -X POST -d '{
      "transition": {
        "id": "31"
      }
    }' https://tip-of-the-week.atlassian.net.example.com/rest/api/2/issue/REST-1/transitions

    curl -u admin -H "Content-Type: application/json" -X POST -d '{
      "transition": {
        "id": "31"
      }
    }' https://tip-of-the-week.atlassian.net.example.com/rest/api/2/issue/REST-2/transitions

    curl -u admin -H "Content-Type: application/json" -X POST -d '{
      "transition": {
        "id": "21"
      }
    }' https://tip-of-the-week.atlassian.net.example.com/rest/api/2/issue/REST-3/transitions


Which gives the following board:

![Result of the issues transitions.](Issues-Moved.png "2 Issues in Done and 1 in In Progress.")

7. Finishing our first sprint
-----------------------------
To round it off we'll finish this sprint, even though one issue hasn't been completed yet.

    curl -u admin -H "Content-Type: application/json" -X POST -d '{
      "state":"closed"
    }' https://tip-of-the-week.atlassian.net.example.com/rest/agile/1.0/sprint/1 | python -m json.tool

This once again returns all the available information for this sprint:

    {
        "completeDate": "2015-12-01T21:30:14.203+01:00",
        "endDate": "2015-12-10T16:22:00.000+01:00",
        "id": 1,
        "name": "Tip of the Week Sprint 1",
        "originBoardId": 1,
        "self": "https://tip-of-the-week.atlassian.net.example.com/rest/agile/1.0/sprint/1",
        "startDate": "2015-12-04T06:22:00.000+01:00",
        "state": "closed"
    }

And as you can see our unfinished issue is back on top of the product backlog.

![Unfinished issue back on top of the product backlog](Issue-back-on-backlog.png "Unfinished issue back on the top of the product backlog")

This is only one of the many things you can do using the JIRA Software REST API.

For those who need something to start here is a small script you can use to create a project and some issues:
<a href="./set-up-test-project.sh">Set-up Test Project Script</a>.
Keep in mind that this is just an example script.

Enjoy the REST API and please share this article if you found it helpful!
