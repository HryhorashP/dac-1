---
title: "Let's build a Bitbucket add-on!"
date: "2015-07-07T14:00:00+07:00"
author: "tpettersen"
categories: ["Atlassian Connect", "bitbucket", "Add-ons"]
---

<style>
  .centered {
    margin: 25px auto;
    display: block;
  }

  .bordered {
    border: 1px solid black;
  }
</style>

A few weeks ago, we introduced Atlassian Connect for Bitbucket, an add-on
framework that lets you modify the Bitbucket UI. This post is a quick
walkthrough on how to build a simple add-on that retrieves data from a
repository, does some simple aggregation and displays it to the user.

Bitbucket add-ons are stand-alone web applications that expose a simple JSON
descriptor describing the end-points and Bitbucket modules that they implement.
In this tutorial we'll build a simple static add-on comprised of a little bit of
HTML, JavaScript and CSS. Though static, it exhibits some powerful dynamic
behaviour by interacting with Bitbucket's REST API using the [cross-domain
JavaScript bridge].

Here's a screenshot of the add-on we'll be building:

<a href="mockup.png"><img class="centered bordered" title="The add-on we'll be building" alt="Screenshot" src="mockup.png"></a>

The add-on creates a new link in the left-hand Bitbucket navigation menu
(labelled *File types* in the screenshot above) and a new repository view that 
shows the types of files present in the repository. For a little bit of visual 
appeal, we'll use a simple word cloud to represent how many files of each type 
are present. The `font-size` for each file type corresponds to the number of 
times it occurs in the repository.

The video tutorial is thirty minutes long, but teaches you everything you need
to know about building a static Bitbucket add-on from scratch. I recommend
viewing it in [full screen mode on YouTube.com] in glorious 1080p:

<iframe class="centered" width="850" height="374" src="https://www.youtube.com/embed/SEWYIQ0iCiA" frameborder="0" allowfullscreen></iframe>

Alternatively, if you'd prefer to dive right in and start hacking on your own 
add-on, you can jump straight to:

- the [source code for the tutorial add-on],
- the [Atlassian Connect for Bitbucket developer docs]; or
- the [Bitbucket add-on 5 minute quickstart].

The add-on in this tutorial is a simple client-side add-on built on HTML, CSS 
and JavaScript. But the coolest thing about Atlassian Connect is that the API 
for building add-ons is simple HTTP! You're free to build an add-on in PHP, 
Java, .Net, Rails, Python, Go, Haskell.. whatever language and framework takes 
your fancy. For some examples of other add-ons, check out the [Find new 
add-ons] section of your Bitbucket account settings.

If you've enjoyed the tutorial, or have questions or comments regarding 
Atlassian Connect, please hit me up on Twitter! I'm [@kannonboy].

[cross-domain JavaScript bridge]: https://developer.atlassian.com/static/bitbucket/concepts/javascript-api.html
[full screen mode on YouTube.com]: https://www.youtube.com/watch?v=SEWYIQ0iCiA
[source code for the tutorial add-on]: https://bitbucket.org/tpettersen/bitbucket-type-cloud
[Atlassian Connect for Bitbucket developer docs]: https://developer.atlassian.com/bitbucket
[Bitbucket add-on 5 minute quickstart]: https://developer.atlassian.com/static/bitbucket/guides/getting-started.html
[Find new add-ons]: https://bitbucket.org/account/addon-directory/
[@kannonboy]: https://twitter.com/kannonboy
