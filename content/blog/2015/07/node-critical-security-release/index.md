---
title: "Critical security releases for Node.js and io.js"
date: "2015-07-09T23:00:00+07:00"
author: "rwhitbeck"
categories: ["nodejs", "iojs", "security"]
---

Late last week, there were new releases for both Node.js and io.js that 
addressed a recent critical security concern. A bug in the V8 JavaScript engine 
was found that could cause a denial of service attack. Vendors with add-ons 
running Node.js or io.js in production may want to upgrade your servers.

The critical security releases for Node.js and io.js include:
* [node.js-v0.12.6](http://nodejs.org/dist/v0.12.6/)
* [io.js-v2.3.3](https://iojs.org/dist/v2.3.3/)
* [io.js-v1.8.3](https://iojs.org/dist/v1.8.3/)

**UPDATE:** *Only a few hours after this post was published was there another 
release of Node.js.  [Version 0.12.7](http://blog.nodejs.org/2015/07/10/node-v0-12-7-stable/) 
upgrades OpenSSL which fixes another security vulnerability. More info on the 
vulnerability can be found on the [OpenSSL site](https://openssl.org/news/secadv_20150709.txt). 
Again, if you're running Node.js in production you'll want to make sure you have 
the latest version.* 

A brief description of the vulnerability from the [Node & JavaScript Medium blog](https://medium.com/node-js-javascript/important-security-upgrades-for-node-js-and-io-js-8ac14ece5852).

> Kris Reeves and Trevor Norris pinpointed a bug in V8 in the way it decodes 
UTF strings. This impacts Node at the Buffer to UTF8 String conversion and can 
cause a process to crash. The security concern comes from the fact that a lot 
of data from outside of an application is delivered to Node via this mechanism 
which means that users can potentially deliver specially crafted input data 
that can cause an application to crash when it goes through this path. We know 
that most networking and file system operations are impacted as would be many 
user-land uses of Buffer to UTF8 String conversion. We know that HTTP(S) header 
parsing is not vulnerable because Node does not convert this data as UTF8. This 
is a small consolation because it restricts the way HTTP(S) can be exploited 
but there is more to HTTP(S) than header parsing obviously. We also have no 
information yet on how the various TLS terminators and forward-proxies in use 
may potentially mitigate against the form of data required for this exploit.

For testing you can switch versions of Node.js/io.js using a CLI tool called 
[nvm](https://github.com/creationix/nvm).

Installing:

```
$ nvm install v0.12.6
######################################################################## 100.0%
Now using node v0.12.6
```

Switch to a version already installed:
```
$ nvm use iojs-v2.3.3
Now using node iojs-v2.3.3
```
Subscribe to [@atlassiandev](http://twitter.com/atlassiandev) and our 
[blog feed](https://developer.atlassian.com/blog/feed.xml) to stay up-to-date 
with the latest information.